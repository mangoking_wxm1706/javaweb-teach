package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import service.UserService;
import service.impl.UserServiceImpl;
import utils.MDGenerator;

public class LoginServlet extends HttpServlet{
  
  private static final long serialVersionUID = 244021452500444424L;

  private UserService us = new UserServiceImpl();
  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html"); 
    PrintWriter out = resp.getWriter();
    String name = req.getParameter("name");  
    String password = req.getParameter("password");  
    if(us.validate(name, MDGenerator.md5(password))) {
      req.getSession().setAttribute("name", name);
      req.getRequestDispatcher("/data.html").forward(req, resp);
    }else {
      out.println("登录失败<hr/>");
      req.getRequestDispatcher("/login.html").include(req, resp);
    }
  }

  public UserService getUs() {
    return us;
  }

  public void setUs(UserService us) {
    this.us = us;
  }
  
}
