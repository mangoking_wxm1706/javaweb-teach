package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import service.UserService;
import service.impl.UserServiceImpl;
import utils.MDGenerator;

import java.io.IOException;
import bean.User;

/**
 * Servlet implementation class RegisterServlet
 */
public class RegisterServlet extends HttpServlet {

  private static final long serialVersionUID = -1485882677997956764L;
  private UserService us = new UserServiceImpl();

  @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    response.setCharacterEncoding("utf-8");
    
    String name = request.getParameter("name");
    String password = request.getParameter("password");
    String email = request.getParameter("email");
    String uid = name.charAt(0) + String.valueOf(System.currentTimeMillis());
    User u = new User();
    u.setUid(uid);
    u.setName(name);
    u.setPassword(MDGenerator.md5(password));
    u.setEmail(email);
    if(us.save(u) != null) {
      response.getWriter().println("注册成功<hr />");
      request.getRequestDispatcher("/login.html").include(request, response);
    }else {
      response.getWriter().println("注册失败<hr />");
      request.getRequestDispatcher("/register.html").include(request, response);
    }
	}

  public UserService getUs() {
    return us;
  }

  public void setUs(UserService us) {
    this.us = us;
  }

}
