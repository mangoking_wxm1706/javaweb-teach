package servlet;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class MyServlet extends HttpServlet{
  /**
   * 
   */
  private static final long serialVersionUID = -5770582923018867141L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.getWriter().println("<h1>Hello World!</h1>");
    
    try {
      DriverManager.getConnection("");
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
