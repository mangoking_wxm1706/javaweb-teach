package servlet;

import java.io.IOException;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class AddServlet extends HttpServlet{
 
  
  /**
   * 
   */
  private static final long serialVersionUID = 244021452500444424L;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("GET");
    resp.getWriter().println(add(req));
  }
  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("POST");
    resp.getWriter().println(add(req));
  }
  
  private double add(HttpServletRequest req) {
    String a = req.getParameter("a");
    String b = req.getParameter("b");
    return Double.parseDouble(a) + Double.parseDouble(b);
  }
}
