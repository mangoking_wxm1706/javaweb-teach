package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import utils.JDBCConnector;


public class DataServlet extends HttpServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 4417329178179076868L;
  
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 

    try(Connection c = JDBCConnector.getConnection()) {
      Statement statem = c.createStatement();
      
      ResultSet rs = statem.executeQuery("select * from user_info");
      while(rs.next()) {
        resp.getWriter().println(rs.getString(2));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    
  }
  
}
