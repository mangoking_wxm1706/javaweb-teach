package servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class MessageServlet extends HttpServlet {

  private static final long serialVersionUID = 1818420990535117329L;

  @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("application/json");
    response.setCharacterEncoding("utf-8");
    
    BufferedReader br = request.getReader();
    JSONObject root = JSON.parseObject(br.readLine());
    System.out.println(root.getIntValue("page"));
    System.out.println(root.getIntValue("size"));
    
    List<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> m = new HashMap<String, String>();
    m.put("msg", "123");
    m.put("name", "555");
    list.add(m);
    response.getWriter().println(JSONObject.toJSONString(list));
	}
  
  @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
