package filter;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

public class ParamFilter implements Filter{

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    
    String a = request.getParameter("a");
    String b = request.getParameter("b");
    if(a == null || b == null || a.equals("") || b.equals("")) {
      response.setCharacterEncoding("GBK");
      response.setContentType("text/html");
      response.getWriter().println("<h1>参数不完整</h1>");
    }else {
      chain.doFilter(request, response);
    }
    
  }
}
