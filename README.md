# javaweb-teach

## 简介
本项目主要用于Java Web开发教学，根目录下的文件夹以数字形式命名，存放同序号视频的源代码。教学视频存放在B站，首地址为：
https://space.bilibili.com/66029745/channel/collectiondetail?sid=2276243&spm_id_from=333.788.0.0。

## 开发环境
- Java版本：Java SE 11
- Tomcat版本：Tomcat 10.1
- 数据库版本：MySQL8.0 或 PostgreSQL 15
- IDE：Eclipse EE
- 第三方包：mysql-connector-j-8.3.0；mybatis-3.5.15；commons-dbcp2-2.12.0；commons-logging-1.3.0；commons-pool2-2.12.0；fastjson-1.2.83

## 视频目录
### 01 Tomcat安装
### 02 Eclipse环境中配置Tomcat服务器
### 03 开发第一个Web程序
### 04 如何传递和获取请求参数
### 05 过滤器的实现与使用
### 06 Session和Cookie的使用
### 07 用户登录与注销
### 08 数据库开发环境配置
### 09 将数据库应用到用户注册
### 10 基于数据库修改登录服务
### 11 应用分层软件架构
### 12 异步请求与JSON格式
### 13 综合实验之留言板
### 15 引入和使用MyBatis框架
### 19 MVC版本的留言板