package spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringContextUtil implements ApplicationContextAware{
  private static ApplicationContext context;
  
  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    context = applicationContext;
  }
  
  public static ApplicationContext getContext() {
    return context;
  }
  
  //获取注册的Bean
  public static Object getBean(String beanName) {
    return getContext().getBean(beanName);
  }
  
  public static <T> T getBean(Class<T> type) {
    return getContext().getBean(type);
  }

}
