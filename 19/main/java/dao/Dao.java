package dao;

import java.io.Serializable;

/**
 * 
 * 通用数据访问接口，用于定义数据访问层的一般化操作，包括增删改查（GRUD）操作。
 * 
 * @author  unascribed
 * 
 */
public interface Dao <T, PK extends Serializable> {
  PK create(T newInstance);
  T read(PK id);
  void update(T transientObject);
  void delete(T persistentObject);
}
