package dao;

import java.util.Optional;

import bean.entity.User;

/**
 * 
 * 用户数据访问接口，是基于{@link Dao}的扩展接口，支持用户的特有操作。
 * 
 * @author  xmwang
 * 
 */

public interface UserDao extends Dao<User, String> {
  Optional<User> findByName(String userName); // 根据名字查找用户，用户名为唯一值，返回结果唯一
}
