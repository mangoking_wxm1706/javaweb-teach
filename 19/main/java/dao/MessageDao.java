package dao;

import java.util.List;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
/**
 * 
 * 用户数据访问接口，是基于{@link Dao}的扩展接口，支持留言的特有操作。
 * 
 * @author  xmwang
 * 
 */
public interface MessageDao extends Dao<Message, String>{
  List<MessageJoinUser> limit(int start, int size);
}
