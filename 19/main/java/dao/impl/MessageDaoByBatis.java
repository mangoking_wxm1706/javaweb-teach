package dao.impl;

import java.util.List;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
import dao.MessageDao;
import orm.inf.MessageMapper;
/**
 * 
 * 该类是对{@link MessageDao}接口的一种实现，采用MyBatis方式。
 * 
 * @author xmwang
 * 
 */
public class MessageDaoByBatis implements MessageDao {
  MessageMapper messageMapper;

  public MessageMapper getMessageMapper() {
    return messageMapper;
  }

  public void setMessageMapper(MessageMapper messageMapper) {
    this.messageMapper = messageMapper;
  }

  @Override
  public String create(Message newInstance) {
    if(messageMapper.insertMessage(newInstance)) {
      return newInstance.getUid();
    }else {
      return null;
    }
  }

  @Override
  public Message read(String id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void update(Message transientObject) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void delete(Message persistentObject) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public List<MessageJoinUser> limit(int start, int size) {
    return messageMapper.selectMesages(start, size);
  }


}
