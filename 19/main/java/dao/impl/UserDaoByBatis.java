package dao.impl;

import java.util.Optional;

import bean.entity.User;
import dao.UserDao;
import orm.inf.UserMapper;
/**
 * 
 * 该类是对{@link UserDao}接口的一种实现，采用MyBatis方式。
 * 
 * @author xmwang
 * 
 */
public class UserDaoByBatis implements UserDao {
  UserMapper userMapper;

  public UserMapper getUserMapper() {
    return userMapper;
  }

  public void setUserMapper(UserMapper userMapper) {
    this.userMapper = userMapper;
  }

  @Override
  public String create(User newInstance) {
    if(userMapper.insertUser(newInstance)) {
      return newInstance.getUid();
    }else {
      return null;
    }
  }

  @Override
  public User read(String id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void update(User transientObject) {
    // TODO Auto-generated method stub

  }

  @Override
  public void delete(User persistentObject) {
    // TODO Auto-generated method stub

  }

  @Override
  public Optional<User> findByName(String userName) {
    User u = userMapper.selectUser(userName);
    return Optional.of(u);
  }

}
