package orm.inf;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import bean.entity.User;
/**
 * 
 * 用户映射器接口，与UserMapper.xml一起定义用户表的映射器。
 * 
 * @author  xmwang
 * 
 */
public interface UserMapper {
  @Select("select * from user_info where name = #{name}")
  User selectUser(@Param("name") String name);
  @Insert("insert into user_info (uid,name,password,email) values (#{uid},#{name},#{password},#{email})")
  boolean insertUser(User user);
}
