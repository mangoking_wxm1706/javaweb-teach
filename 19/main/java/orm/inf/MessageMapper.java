package orm.inf;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
/**
 * 
 * 留言映射器接口，与MessageMapper.xml一起定义留言表的映射器。
 * 
 * @author  xmwang
 * 
 */
public interface MessageMapper {
  // 多个不同参数，通过Param注解进行区分
  @Select("select msgid, message.uid,name,content,time from message, user_info where message.uid = user_info.uid order by time desc limit #{start},#{size}")
  List<MessageJoinUser> selectMesages(@Param("start") int start, @Param("size") int size);
  @Insert("insert into message (msgid,uid,content,time) values (#{msgid},#{uid},#{content},#{time})")
  boolean insertMessage(Message msg);
}
