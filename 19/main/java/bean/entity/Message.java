package bean.entity;

import java.sql.Timestamp;

/**
 * 
 * 留言实体类，对应于数据库中的留言数据表。
 * 
 * @author  xmwang
 * 
 */

public class Message {
  
  private String msgid;
  private String uid;
  private String content;
  private Timestamp time;
  
  public Message(String msgid, String uid, String content, Timestamp time) {
    super();
    this.msgid = msgid;
    this.uid = uid;
    this.content = content;
    this.time = time;
  }
  
  public String getMsgid() {
    return msgid;
  }
  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }
  public String getUid() {
    return uid;
  }
  public void setUid(String uid) {
    this.uid = uid;
  }
  public String getContent() {
    return content;
  }
  public void setContent(String content) {
    this.content = content;
  }
  public Timestamp getTime() {
    return time;
  }
  public void setTime(Timestamp time) {
    this.time = time;
  }
}
