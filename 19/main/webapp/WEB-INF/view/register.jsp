<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>注册页</title>
</head>
<body>
	<form action="./registersubmit" method="post" modelAttribute="userInfo">
		用户名：<input id="name" type="text" name="name" path="name"/>
		密码：<input id="password" type="password" name="password" path="password"/>
		确认密码：<input id="confirmPassword" type="password" name="confirmPassword"/>
		邮箱：<input id="email" type="text" name="email" path="email"/>
		<input type="submit" value="注册"/>
	</form>
</body>
</html>