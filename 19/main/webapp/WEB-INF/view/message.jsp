<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>留言板</title>
	<link rel="stylesheet" href="../style.css">
	<script type = "text/javascript" src="../utils.js"></script>
	<script type = "text/javascript">
	  function listMessages(data){
		  var msglist = document.getElementById('msglist');
		  msglist.innerHTML = "";
		  for(var i in data){
			  var div = document.createElement("div");
			  var name = document.createElement("a");
			  var time = document.createElement("span");
			  if(i % 2 == 0){
				  div.className = "container";
				  name.className = "right";
				  time.className = "time-left";
			  }else{
				  div.className = "container darker";
				  time.className = "time-right";
			  }
			  var para = document.createElement("p");
			  para.innerText = data[i].content;
			  name.innerHTML = data[i].name;
			  time.innerHTML = formatter(data[i].time)
			  div.appendChild(name);
			  div.appendChild(para);
			  div.appendChild(time);
			  msglist.appendChild(div);
		  }
	  }
	  function loadList(page){
		  postData("./message/get",{page:page})
	      .then((data) => {
	        listMessages(data);
      	})
	  }
	  function submit(){
		  var name = document.getElementById('username').innerHTML;
		  if(name != "游客"){
			  postData("./message/add", {content:document.getElementById('content').value})
		        .then((data) =>{
		        	if(data.posted == true){
		        		document.getElementById('content').value = '';
		        		loadList(1); // 提交后更新列表
		        	}
		        });
		  }else{
			  alert("游客不能提交留言");
		  }
	  }
	  function setbg(color){
		  document.getElementById('content').style.background = color;
	  }
	</script>
</head>
<body onload="loadList(1)">
<div align="center">你好,<span id="username">${username}</span> <button id="ubtn" onclick="location.href=${actionurl}">${actionname}</button></div>
<h1 align="center">欢迎留言</h1>
<div class='main' id='main'>
	<div class='msgeditor'>
	  <textarea id='content' onfocus="setbg('#e5fff3')" onblur="setbg('white')"></textarea>
	  <button class="publish" onclick="submit()">提交</button>
	</div>
	<div id='msglist'></div>
	<div id='reload'><button style="width:100%;height:40px" onclick="">加载</button></div>
</div>
</body>
</html>