package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet{
 
  
  /**
   * 
   */
  private static final long serialVersionUID = 244021452500444424L;

  
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html"); 
    PrintWriter out = resp.getWriter();
    String name = req.getParameter("name");  
    String password = req.getParameter("password");  
    if(name.equals("admin") && password.equals("admin@123")) {
      req.getSession().setAttribute("name", name);
      req.getRequestDispatcher("/data.html").forward(req, resp);
    }else {
      out.println("登录失败<hr/>");
      req.getRequestDispatcher("/login.html").include(req, resp);
    }
  }

}
