package servlet;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CookieServlet extends HttpServlet{

  /**
   * 
   */
  private static final long serialVersionUID = 2694988566366589088L;
  
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    req.getSession().setAttribute("name", "xiaoming");
    
    System.out.println(req.getSession().getAttribute("name"));
  }
  
  private String getCookieValue(Cookie[] cookies, String name) {
    String v = null;
    if(cookies != null) {
      for(Cookie cookie : cookies) {
        if(cookie.getName().equals(name)) {
          v = cookie.getValue();
        }
      }
    }
    return v;
  }
  
}
