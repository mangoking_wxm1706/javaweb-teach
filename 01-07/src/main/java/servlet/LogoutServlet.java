package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class LogoutServlet extends HttpServlet{
 
  
  /**
   * 
   */
  private static final long serialVersionUID = 244021452500444424L;

  
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");  
    PrintWriter out = resp.getWriter();
    
    //req.getSession().removeAttribute("name");
    req.getSession().invalidate();
    
    out.println("用户已注销<hr />");
    req.getRequestDispatcher("login.html").include(req, resp);
  }

}
