package orm.inf;

import bean.entity.User;
/**
 * 
 * 用户映射器接口，与UserMapper.xml一起定义用户表的映射器。
 * 
 * @author  xmwang
 * 
 */
public interface UserMapper {
  User selectUser(String name);
  boolean insertUser(User user);
}
