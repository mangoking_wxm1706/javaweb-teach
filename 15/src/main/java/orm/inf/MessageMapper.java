package orm.inf;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
/**
 * 
 * 留言映射器接口，与MessageMapper.xml一起定义留言表的映射器。
 * 
 * @author  xmwang
 * 
 */
public interface MessageMapper {
  // 多个不同参数，通过Param注解进行区分
  List<MessageJoinUser> selectMesages(@Param("start") int start, @Param("size") int size);
  boolean insertMessage(Message msg);
}
