package orm;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
/**
 * 
 * 工具类，管理{@link SqlSessionFactory}单例。
 * 
 * @author  xmwang
 * 
 */
public class MySessionFactory {
  private static SqlSessionFactory ssf = null;
  static {
    ClassLoader cl = MySessionFactory.class.getClassLoader();
    String path = "orm/mybatis-config.xml";
    try(InputStream inputStream = Resources.getResourceAsStream(cl, path)){
      ssf = new SqlSessionFactoryBuilder().build(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  private MySessionFactory() {}
  public static SqlSessionFactory getSessionFactory() {
    return ssf;
  }
}
