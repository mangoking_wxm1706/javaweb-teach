package service;

import java.util.List;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
/**
 * 
 * 留言业务接口。
 * 
 * @author xmwang
 * 
 */
public interface MessageService {
  boolean post(Message msg); // 判断是否创建成功
  List<MessageJoinUser> getMessages(int pageSize, int pageIndex);
}
