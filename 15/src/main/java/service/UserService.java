package service;

import java.util.Optional;

import bean.entity.User;
/**
 * 
 * 用户业务接口。
 * 
 * @author xmwang
 * 
 */
public interface UserService {
  public Optional<String> validate(String name, String password); // 验证成功返回用户ID
  public Optional<String> register(User u); //返回用户ID
}
