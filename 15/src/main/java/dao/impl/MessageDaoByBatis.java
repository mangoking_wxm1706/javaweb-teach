package dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
import dao.MessageDao;
import orm.MySessionFactory;
import orm.inf.MessageMapper;
/**
 * 
 * 该类是对{@link MessageDao}接口的一种实现，采用MyBatis方式。
 * 
 * @author xmwang
 * 
 */
public class MessageDaoByBatis implements MessageDao {

  @Override
  public String create(Message newInstance) {
    try(SqlSession ss = MySessionFactory.getSessionFactory().openSession(true)){
      MessageMapper mm = ss.getMapper(MessageMapper.class);
      if(mm.insertMessage(newInstance)) {
        return newInstance.getUid();
      }else {
        return null;
      }
    }
  }

  @Override
  public Message read(String id) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void update(Message transientObject) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void delete(Message persistentObject) {
    // TODO Auto-generated method stub
    
  }

  @Override
  public List<MessageJoinUser> limit(int start, int size) {
    try(SqlSession ss = MySessionFactory.getSessionFactory().openSession(true)){
      MessageMapper mm = ss.getMapper(MessageMapper.class);
      return mm.selectMesages(start, size);
    }
  }


}
