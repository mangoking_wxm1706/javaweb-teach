package dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;

import java.util.ArrayList;

import dao.MessageDao;
import utils.JDBCConnector;
/**
 * 
 * 该类是对{@link MessageDao}接口的一种实现，采用普通的JDBC方式。
 * 
 * @author xmwang
 * 
 */
public class MessageDaoImpl implements MessageDao{
  
  private static final Log log = LogFactory.getLog(MessageDaoImpl.class);

  @Override
  public String create(Message msg) {
    try(Connection conn = JDBCConnector.getConnection()){
      PreparedStatement pstmt = conn.prepareStatement("insert into message values(?,?,?,?)");
      pstmt.setString(1, msg.getMsgid());
      pstmt.setString(2, msg.getUid());
      pstmt.setString(3, msg.getContent());
      pstmt.setTimestamp(4, msg.getTime());
      pstmt.executeUpdate();
      return msg.getMsgid();
    } catch (SQLException e) {
      log.warn("消息创建失败");
    }
    return null;
  }

  @Override
  public Message read(String id) {
    return null;
  }

  @Override
  public void update(Message transientObject) {
   
  }

  @Override
  public void delete(Message persistentObject) {
    
  }

  @Override
  public List<MessageJoinUser> limit(int start, int size) {
    List<MessageJoinUser> list = new ArrayList<MessageJoinUser>();
    try(Connection conn = JDBCConnector.getConnection()){
      PreparedStatement pstmt = conn.prepareStatement("select msgid,message.uid,name,content,time from message, user_info where message.uid = user_info.uid order by time desc limit ? , ?");
      pstmt.setInt(1, start);
      pstmt.setInt(2, size);
      ResultSet rs = pstmt.executeQuery();
      while(rs.next()) {
        list.add(new MessageJoinUser(rs.getString(1), rs.getString(2),rs.getString(3),rs.getString(4),rs.getTimestamp(5)));
      }
    } catch (SQLException e) {
      log.warn("查询消息失败");
    } 
    return list;
  }

}
