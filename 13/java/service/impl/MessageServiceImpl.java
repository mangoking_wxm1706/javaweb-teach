package service.impl;

import java.util.List;

import bean.entity.Message;
import bean.transfer.MessageJoinUser;
import service.MessageService;
import dao.MessageDao;
import dao.impl.MessageDaoImpl;
/**
 * 
 * 该类是对{@link MessageService}接口的一种实现，需要注入一种{@link MessageDao}实现。
 * 
 * @author xmwang
 * 
 */
public class MessageServiceImpl implements MessageService{
  
  private MessageDao md = new MessageDaoImpl(); 
  
  @Override
  public boolean post(Message msg) {
    if(md.create(msg) != null) {
      return true;
    }
    return false;
  }

  @Override
  public List<MessageJoinUser> getMessages(int pageSize, int pageIndex) {
    int start = pageSize * (pageIndex - 1);
    return md.limit(start, pageSize);
  }

}
