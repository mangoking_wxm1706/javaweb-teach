package service.impl;

import java.util.Optional;

import bean.entity.User;
import dao.UserDao;
import dao.impl.UserDaoImpl;
import service.UserService;

/**
 * 
 * 该类是对{@link UserService}接口的一种实现，需要注入一种{@link UserDao}实现。
 * 
 * @author xmwang
 * 
 */

public class UserServiceImpl implements UserService{
  private UserDao ud = new UserDaoImpl();

  @Override
  public Optional<String> validate(String name, String password) {
    Optional<String> uid = Optional.empty();
    Optional<User> u = ud.findByName(name);
    if(u.isPresent()) {
      if(u.get().getPassword().equals(password)) {
        uid = Optional.of(u.get().getUid());
      }
    } 
    return uid;
  }

  @Override
  public Optional<String> register(User u) {
    // 返回的主键可能为空，表示注册失败
    Optional<String> id = Optional.ofNullable(ud.create(u));
    return id;
  }

}
